package tech.hidetora.dynamicfilter.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;
import tech.hidetora.dynamicfilter.dto.StudentDto;
import tech.hidetora.dynamicfilter.entity.Student;
import tech.hidetora.dynamicfilter.repository.StudentRepository;

@Service
@Transactional
public class StudentService {
    private final StudentRepository repository;

    public StudentService(StudentRepository repository) {
        this.repository = repository;
    }

    public StudentDto save(StudentDto studentDto) {
        Student student = StudentDto.toEntity(studentDto);
        repository.save(student);
        return StudentDto.fromEntity(student);
    }

    public StudentDto getStudent(Long id) {
        Student student = repository.findById(id).get();
        return StudentDto.fromEntity(student);
    }

    public List<StudentDto> getAllStudents() {
        List<Student> students = repository.findAll();
        return students.stream().map(s -> StudentDto.fromEntity(s)).toList();
    }



}
