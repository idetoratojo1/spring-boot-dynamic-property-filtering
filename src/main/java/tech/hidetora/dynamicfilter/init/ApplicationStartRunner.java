package tech.hidetora.dynamicfilter.init;

import java.util.List;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import tech.hidetora.dynamicfilter.dto.StudentDto;
import tech.hidetora.dynamicfilter.entity.School;
import tech.hidetora.dynamicfilter.entity.Student;
import tech.hidetora.dynamicfilter.repository.SchoolRepository;
import tech.hidetora.dynamicfilter.repository.StudentRepository;
import tech.hidetora.dynamicfilter.service.StudentService;

@Component
@Slf4j
@RequiredArgsConstructor
public class ApplicationStartRunner implements CommandLineRunner {
    private final StudentRepository studentRepository;
    private final SchoolRepository schoolRepository;


    @Override
    public void run(String... args) throws Exception {
        log.info("ApplicationStartRunner.run() is called");

        List.of("IUT", "FGI", "Polytech", "CHU", "Traveaux", "CUS")
                .forEach(name -> {
                    School school = School.builder()
                            .name(name)
                            .email(name.toLowerCase() + "@kingdom.com")
                            .phone("1234567890")
                            .country("China")
                            .createdOn(java.time.LocalDateTime.now().minusYears(30))
                            .zip("12345")
                            .build();
                    schoolRepository.save(school);
                });

        List.of("Hidetora", "Shin", "Sei", "Ten", "Shobunkun", "Seikyo", "Yotanwa", "Oki", "Kanki", "Osen")
            .forEach(name -> {
                Student studentDto = Student.builder()
                    .name(name)
                    .email(name.toLowerCase() + "@kingdom.com")
                    .phone("1234567890")
                    .address("123 Main St")
                    .city("Kanyou")
                    .school(schoolRepository.findByName("IUT").get())
                    .state("Qin")
                    .country("China")
                    .zip("12345")
                    .dob(java.time.LocalDate.now())
                    .pob("Kantan, Qin, China")
                    .build();
                studentRepository.save(studentDto);
            });
    }

}
