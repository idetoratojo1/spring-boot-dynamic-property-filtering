package tech.hidetora.dynamicfilter.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import tech.hidetora.dynamicfilter.entity.Student;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class StudentDto {
    private Long id;
    private String name;
    private String email;
    private String phone;
    private String address;
    private String city;
    private String state;
    private String country;
    private SchoolDto school;
    private String zip;
    private LocalDate dob;
    private String pob;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

    public static Student toEntity(StudentDto studentDto) {
        return Student.builder()
                .id(studentDto.getId())
                .name(studentDto.getName())
                .email(studentDto.getEmail())
                .phone(studentDto.getPhone())
                .address(studentDto.getAddress())
                .city(studentDto.getCity())
                .school(SchoolDto.toEntity(studentDto.getSchool()))
                .state(studentDto.getState())
                .country(studentDto.getCountry())
                .zip(studentDto.getZip())
                .dob(studentDto.getDob())
                .pob(studentDto.getPob())
                .createdAt(studentDto.getCreatedAt())
                .updatedAt(studentDto.getUpdatedAt())
                .build();
    }

    public static StudentDto fromEntity(Student student) {
        return StudentDto.builder()
                .id(student.getId())
                .name(student.getName())
                .email(student.getEmail())
                .phone(student.getPhone())
                .address(student.getAddress())
                .city(student.getCity())
                .state(student.getState())
                .school(SchoolDto.fromEntity(student.getSchool()))
                .country(student.getCountry())
                .zip(student.getZip())
                .dob(student.getDob())
                .pob(student.getPob())
                .createdAt(student.getCreatedAt())
                .updatedAt(student.getUpdatedAt())
                .build();
    }

}
