package tech.hidetora.dynamicfilter.dto;

import lombok.*;
import tech.hidetora.dynamicfilter.entity.School;
import tech.hidetora.dynamicfilter.entity.Student;

import javax.persistence.OneToMany;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SchoolDto {
    private Long id;
    private String name;
    private String email;
    private String phone;
    private String city;
    private String country;
    private List<StudentDto> students;
    private String zip;
    private LocalDateTime createdOn;

    public static School toEntity(SchoolDto studentDto) {
        return School.builder()
                .id(studentDto.getId())
                .name(studentDto.getName())
                .email(studentDto.getEmail())
                .phone(studentDto.getPhone())
                .city(studentDto.getCity())
                .country(studentDto.getCountry())
                .students(new ArrayList<>())
                .zip(studentDto.getZip())
                .createdOn(studentDto.getCreatedOn())
                .build();
    }

    public static SchoolDto fromEntity(School school) {
        return SchoolDto.builder()
                .id(school.getId())
                .name(school.getName())
                .email(school.getEmail())
                .phone(school.getPhone())
                .city(school.getCity())
                .country(school.getCountry())
                .students(new ArrayList<>())
                .zip(school.getZip())
                .createdOn(school.getCreatedOn())
                .build();
    }
}
