package tech.hidetora.dynamicfilter.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

import tech.hidetora.dynamicfilter.dto.StudentDto;
import tech.hidetora.dynamicfilter.service.StudentService;

@RestController
@RequestMapping("/api/v1/students")
public class StudentController {
    private final StudentService service;

    public StudentController(StudentService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    public ResponseEntity<StudentDto> getStudent(@PathVariable Long id, @RequestParam(name = "fields", required = false) String fields) {
        StudentDto student = service.getStudent(id);
        return ResponseEntity.ok(student);
    }

    @GetMapping
    public ResponseEntity<List<StudentDto>> getAllStudents(@RequestParam(name = "fields", required = false) String fields) {
        List<StudentDto> allStudents = service.getAllStudents();
        return ResponseEntity.ok(allStudents);
    }

    @PostMapping
    public ResponseEntity<List<StudentDto>> saveStudent(@RequestBody StudentDto studentDto) {
        return ResponseEntity.ok(service.getAllStudents());
    }

}
