package tech.hidetora.dynamicfilter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tech.hidetora.dynamicfilter.entity.School;
import tech.hidetora.dynamicfilter.entity.Student;

import java.util.Optional;

public interface SchoolRepository extends JpaRepository<School, Long> {
    Optional<School> findByName(String name);
}
