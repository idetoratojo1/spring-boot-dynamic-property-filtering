package tech.hidetora.dynamicfilter.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import tech.hidetora.dynamicfilter.entity.Student;

public interface StudentRepository extends JpaRepository<Student, Long> {

}
