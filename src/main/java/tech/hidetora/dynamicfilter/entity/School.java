package tech.hidetora.dynamicfilter.entity;

import com.fasterxml.jackson.annotation.JsonFilter;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonFilter("SchoolFilter")
public class School {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY )
    private Long id;
    private String name;
    private String email;
    private String phone;
    private String city;
    private String country;
    @OneToMany(mappedBy = "school")
    private List<Student> students;
    private String zip;
    private LocalDateTime createdOn;
}
